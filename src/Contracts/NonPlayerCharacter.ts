import { IRollsInitiative } from './IRollInitiative';
import { PhysicalCharacter } from './PhysicalCharacter';

export class NonPlayerCharacter extends PhysicalCharacter implements IRollsInitiative {
;
; 
    public initiativeDice: number;
    // tslint:disable-next-line
    private _random: Function;

    constructor(random:(() => number)|null) {
        if (!random) {
            throw new Error("Random must not be null!")
        }
        super();
        this._random = random;
        this.initiativeDice = 1;
    }
    public getDefaultInitiative() {
        return this.rollInitiative();
    }
    public rollInitiative() {
        let initiative = this.reaction + this.intuition;
        let i = 0;
        while (i < this.initiativeDice) {
          initiative += this._random();
          i++; 
        }
        return initiative;
    }
}