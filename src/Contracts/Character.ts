export abstract class Character {
    public name: string;
    public abstract getDefaultInitiative(): number;
}