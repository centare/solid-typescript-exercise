import { Character } from './Character';

export abstract class PhysicalCharacter extends Character {
    public reaction: number;
    public intuition: number;
}