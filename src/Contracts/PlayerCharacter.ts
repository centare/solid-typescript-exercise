import { PhysicalCharacter } from './PhysicalCharacter';

export class PlayerCharacter extends PhysicalCharacter {
    public getDefaultInitiative(){
        return 0;
    };
}