import {PlayerCharacter} from '../Contracts/PlayerCharacter';

describe(`Player Character`, () => {
    it(`returns 0 when getDefaultInitiative is called.`, () => {
        const actual = new PlayerCharacter();
        const result = actual.getDefaultInitiative();
        expect(result).toEqual(0);
    });
})