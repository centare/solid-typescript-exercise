import { InitiativePassSlot } from './Contracts/InititiativePassSlot';
import { ICombatAction } from './ICombatAction';

export class SimpleCombatAction implements ICombatAction {
    public performAction(character: InitiativePassSlot) {
        if (character.actionsLeft > 0) {
            character.actionsLeft--;
        }
        else {
            throw new Error("Character does not have enough action points")
        }
        return;
    }
}