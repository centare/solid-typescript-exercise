import { InitiativePassSlot } from './Contracts/InititiativePassSlot';
import { ICombatAction } from './ICombatAction';

export class FreeCombatAction implements ICombatAction {
    public performAction(character: InitiativePassSlot) {
        if (character.hasTakenFreeAction) {
            throw new Error("Character has already taken free action")
        }
        character.hasTakenFreeAction = true;
    }
}