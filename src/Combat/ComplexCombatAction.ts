import { InitiativePassSlot } from './Contracts/InititiativePassSlot';
import { ICombatAction } from './ICombatAction';

export class ComplexCombatAction implements ICombatAction {
    public performAction(character: InitiativePassSlot) {
        if (character.actionsLeft === 2) {
            character.actionsLeft = 0;
        }
        else {
            throw new Error("Character does not have enough action points")
        }
        return;
    }
}